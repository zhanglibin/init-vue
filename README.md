#### 基础服务器配置完毕, 旨在快速开发，直接编写业务逻辑代码即可。
1. axios
2. vuex
3. element-ui


# init-vue

> A Vue.js project

## Build Setup

``` bash
# 全局安装 vue-cli
$ npm install --global vue-cli
# 创建一个基于 webpack 模板的新项目
$ vue init webpack my-project
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test

#使用element-ui
npm i element-ui -S
#在main.js中写入
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

#http库
npm install axios
#在main.js中写入
import axios from 'axios'
Vue.prototype.$axios = axios

#vuex
npm install vuex --save
#在main.js 中写入
import store from './store'
new Vuejs({
	store
})


```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
